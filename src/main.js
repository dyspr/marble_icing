var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var numOfCircles = 1500
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < numOfCircles; i++) {
    fill((255 / 4) * (Math.floor(numOfCircles * 0 + 0 * frameCount + i) % 4))
    stroke((255 / 4) * (Math.floor(numOfCircles * 0 + 0 * frameCount + i) % 4))
    strokeWeight(2)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    rotate(frame)
    translate(sin(Math.PI * 2 * (i / 60)) * initSize * boardSize * ((i / (180 + 30 * sin(frame * 12 + i * 0.101)))), cos(Math.PI * 2 * (i / 60)) * initSize * boardSize * ((i / (180 + 30 * sin(frame * 12 + i * 0.101)))))
    ellipse(0, 0, boardSize * initSize * (0.5 + i / numOfCircles))
    pop()
    pop()
  }

  frame += deltaTime * 0.0001

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
